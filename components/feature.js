import React, { Component } from 'react'
import Link from 'next/link'

const Feature = (props) => (
  <figure>
    <div className="iconbase">{props.image}</div>
    <h3>{props.title}</h3>
    <figcaption>{props.description}</figcaption>
    <style jsx>{`
      .iconbase{
        background:rgba(154, 157, 171, 0.1);
        border-radius:8px;
        padding:20px;
      }
      h3 {
        margin: 20px 0 8px;
      }
      figcaption{
        font-size:14px;
      }
      figure {
        text-align:left;
      }
    `}</style>
  </figure>
)

export default Feature
